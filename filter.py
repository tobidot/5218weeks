import panflute as pf


def action(elem, doc):
    if isinstance(elem, pf.Image) and elem.url[0] == '/':
        elem.url = './static' + elem.url
        return elem


if __name__ == '__main__':
    pf.run_filter(action)
