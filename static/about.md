# What is this?

If you would become 100 years old your life would have 5218 weeks.
It's probably gonna be less than that, and if not: good for you.
So why not put your life-in-weeks on a 62x110 cm poster.
You can download the PDF for your personalized calendar above.

## Example

Here is an example for someone born on the *14th of May 1993*.
On the very top of the poster you see "Don't Panic!" in big friendly letters.

![Zoom in: Top of the poster](/img/guide_boxes.png)

The main part consists of a grid with 100 rows, one row per year, with each row containing one square per week.
Each row starts with your age in the first square.
Since your zero years old at birth, the first row starts with zero.
The very first week-square is your birthday.
Every week-square has the month in the upper left corner and the day on which the week starts in the lower right.
So for someone born on the *14th of May 1993*, the very first box has *5* in the top left corner and *14* in the lower right corner.
Since the calendar starts with your birthday, *the date of every box falls on the weekday of your birth*.

![Zoom in: New Year marks](/img/guide_new_year.png)

Since every row starts with the first week after your birthday, new-years is usually somewhere in the middle of the row.
The very first week starting after a new year contains the year number.

So far you could get the impression that this calendar is just something to look at.
But actually you should think of the calendar as a "colouring-book" (details below).
In short: As you live your life you colour every week with some schema with the circles on the right serving as a legend.

![Zoom in: Legend bubbles](/img/guide_legend.png)

There are [several ways](https://waitbutwhy.com/2014/05/life-weeks.html) to do the colouring.
The way I do it is:
I defined broad categories like *Music*, *Friends*, *Adventure*, *Work*, *Family*, and so on.
Each category written in one of the circles on the right, and the circle is coloured with a certain colour.
*Make sure you'll have enough pencils left of that color as you'll need them until you die.*
After every week I reflect on the past week and colour the week according to the category which was most important to me in that week.
**Not with the category I spent most time with**, since this would often be work or sleep.
Since I still have steady hands, I also add a little note or sketch in the week-square, to remind me off the reason for the specific choice of colour.
However I guess from row 80 on I'll have some problems fitting the notes in the box.


One note of **caution**:
The PDF takes quite some while to render, even though it's very small.
However it renders eventually.
If it doesn't render, please create an issue on [Gitlab](https://gitlab.com/tobidot/5218weeks/issues).


# Raison d’Être

In our society today it's way to easy to distract ourselves from the basic fact that we're all gonna die at some point in time.
Or as Charlie Winston [put it](https://www.youtube.com/watch?v=UBfK3j6Hs00) "we all kick the bucket in the end".
What each and everyone of us makes of this fact is everyone's own business.
However, I think it's generally a good thing to really understand the limitedness of one's own life both intellectually and emotionally to gain a bigger perspective on life.
Understanding the limits of life intellectually is quite easy:
"We get born. We live. We die."
If you understand those three sentences you can check the intellectual understanding off your bucket list.

The more interesting bit is how to understand this emotionally.
Some people think it's enough to put "Memento mori" on a sign, put it in your house and it's done.
However, this sentence doesn't trigger any deep emotions in me.
Other people try to convert the lifespan, we normally count in years, in smaller units to get a better grasp:

    100 years
    = 1200 months
    = 5128 weeks
    = 876600 hours
    = 52596000 minutes
    = 3155760000 seconds

Obviously this doesn't help at all.
If you say *100 years*, you can quite easily imagine 100 of some unit.
Yet, the unit year, even if you life in a place with seasons, is slightly to long to get a feeling for what 100 of those are.
With 1200 months it's the other way around.
A month is more or less graspable, but at least I don't really have a good feeling for what 1200 of something is.
Just try it out: 
What would 1200 pencils look like.
*I don't know. A lot of pencils I guess.*
If you measure life in even smaller units (weeks, hours, minutes, seconds) the unit itself becomes ever more graspable but the amount just becomes emotionally meaningless.

A way smarter approach is to somehow visualize your lifespan.
A brilliant example of this is the blog post of Nathan Yau [Years You Have Left to Live, Probably](https://flowingdata.com/2015/09/23/years-you-have-left-to-live-probably/), where you can see a beautiful animation of your life-expectancy trial-by-trial.
It's quite amazing how it feels when you set your *age* and *gender* and follow the little points representing one version of your life.
Mostly the point drops dead at an age which is in a comfortable distance from your own.
But when one of them drops in 2 years time from now, you really get worried.
Thus, this animation triggers an emotional response.

For myself I wanted to have something that lets me understand the limits of my life emotionally, acts as a constant reminder, and is aesthetically pleasing enough to put it in my room.
So programmed the calendar for myself which you can download above.
Here every week is represented by a 1-by-1 cm square, with one row representing a whole year, from birthday to birthday.
Using this representation your life fits on $0.54m^2$.
That's small enough to get a feeling for the limits of life and to fit on a door (that's were I have mine).
I cannot claim that this representation was my own idea, but I wasn't able to find the blog post that sparked the idea.
Even though it wasn't [Wait but why - Your life in weeks](https://waitbutwhy.com/2014/05/life-weeks.html) (I only found them after I had programmed mine) I recommend reading their article as well.

I consciously decided against a digital version ([Mori Clock](http://moriclock.com/)) of the calendar, as I don't think of it as a tool for personal statistics or self-optimization, but rather a tool for self-reflection.
My main inspiration was the life-carpet mentioned in the children book [The 13½ Lives of Captain Bluebear](https://en.wikipedia.org/wiki/The_13%C2%BD_Lives_of_Captain_Bluebear).
There, Qwerty Uiop (in German Version Qwert Zuiopü) uses a carpet as his life journal.
For me the important part here is that knitting takes a lot of time and so does colouring 1x1 squares by hand.
This makes it ideal for contemplation.
Of course a poster is a horrible data-storage to analyze, especially with a computer, but that's not the goal.
You should think of it more as meditation.
You don't actively analyze your life.
You just create some space for becoming aware of how you live your life while living it.
This awareness comes from reflection, and by having a slow analog process like knitting or colouring 1-by-1 squares you make sure that your mind gets enough space (gets bored enough) to start reflecting.
I guess it's similar to the beautiful [Visual Biography of Bret Victor](http://worrydream.com/).

I hope you enjoy your personalized Life Calendar. Make the best out of it.

---

*2021-06-06 Update*:

Further watching: [5200 Wochen - Was machst du mit deinem Leben? Eine Perspektive](https://youtu.be/w9J6D4r30HY) ([English Version](https://youtu.be/JXeJANDKwDc)) produced by [kurzgesagt](https://kurzgesagt.org/).

This is a really nice video on viewing your whole life in perspective by looking at it at the scale of weeks.
Hence, if you like the idea of the calendar or even already printed one, it's definitly worth while to watch this video.
Thanks to Maxi for pointing me to this video.


---

This project uses:

 - [fontawesome](https://fontawesome.com) ([License](https://fontawesome.com/license)) for the *download-spinner* and *brain* icon
 - [datepicker](https://github.com/fengyuanchen/datepicker) from Fengyuan Chen
 - [diagrams](https://diagrams.github.io/) for the calender creation
 - [milligram](https://milligram.io/) as css framework 
 - [scotty](https://github.com/scotty-web/scotty) as web-framework
 - [haskell](https://www.haskell.org/) as main programing language
