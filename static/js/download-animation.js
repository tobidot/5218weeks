// src: https://stackoverflow.com/a/48956248/3534840


// form submit event
window.onload = function() {
	function setCookie(name, value, expiresInSeconds) {
	    var exdate = new Date();
	    exdate.setTime(exdate.getTime() + expiresInSeconds * 1000);
	    var c_value = escape(value) + ((expiresInSeconds == null) ? "" : "; expires=" + exdate.toUTCString());
	    document.cookie = name + "=" + c_value + '; path=/';
	};
	function getCookie(name) {
	    var parts = document.cookie.split(name + "=");
	    if (parts.length == 2) return parts.pop().split(";").shift();
	};
	function expireCookie(name) {
	    document.cookie = encodeURIComponent(name) + "=; path=/; expires=" + new Date(0).toUTCString();
	};

	var downloadTimer;  // reference to timer object

	function startDownloadChecker(buttonId, imageId, timeout) {
	    var cookieName = "DownloadStatus";
	    var downloadTimerAttempts = timeout;    // seconds

	    setCookie(cookieName, "loading", downloadTimerAttempts);
	    document.forms['birthdate'].submit();

	    // set timer to check for cookie every second
	    downloadTimer = window.setInterval(function () {
		var cookie = getCookie(cookieName);

		// if cookie doesn't exist, or attempts have expired, re-enable form
		if ((typeof cookie === 'undefined') || (cookie == 'done') || (downloadTimerAttempts == 0)) {
		    $("#" + buttonId).removeAttr("disabled");
		    $("#" + imageId).hide();
		    $("#txtSubmit").show();
		    document.getElementById('txtSubmit').style.visibility = 'visible';
		    window.clearInterval(downloadTimer);
		    expireCookie(cookieName);
		}

		downloadTimerAttempts--;
	    }, 1000);
	};

	$("#btnSubmit").click(function () {
	    $(this).attr("disabled", "disabled");  // disable form submit button
	    document.getElementById('txtSubmit').style.visibility = 'hidden';
	    $("#imgLoading").show();  // show loading animation
	    startDownloadChecker("btnSubmit", "imgLoading", 120);
	});
};
