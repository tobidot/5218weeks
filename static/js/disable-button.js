$(document).ready(function(){
	// parse a date in yyyy-mm-dd format
	function parseDate(input) {
	  var parts = input.split('-');
	  // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
	  return new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based
	}

	$('#btnSubmit').attr('disabled',true);
	$('#bday').bind('input', function() {
		if(!moment($(this).val(), 'YYYY-MM-DD', true).isValid())
			$('#btnSubmit').attr('disabled', true);
		else
			$('#btnSubmit').attr('disabled', false);
    })
});
