// on scroll, 
$(window).on('scroll',function(){

    var about = $('#about').offset().top
    // we round here to reduce a little workload
    var stop = Math.round($(window).scrollTop());

    if (stop >= about) {
        $('.navigation').addClass('past-main');
    } else {
        $('.navigation').removeClass('past-main');
    }

});
