build:
	stack build

clean:
	rm .hdevtools.sock
	rm -r .stack-work
	rm *.cabal

css:
	cd static/node_modules/milligram && \
	sass src/milligram.sass dist/my_milligram.css && \
	sass src/milligram.sass dist/my_milligram.min.css --style compressed

pdf:
	pandoc --filter filter.hs --variable urlcolor=blue -s -o about.pdf ./static/about.md
