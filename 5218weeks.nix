{ aeson
, base
, blaze-html
, diagrams
, diagrams-cairo
, diagrams-lib
, directory
, doctemplates
, fetchgit
, hpack
, http-types
, lib
, mkDerivation
, pandoc
, scotty
, scotty-cookie
, split
, text
, time
, wai-extra
, warp
} : mkDerivation {
  pname = "5218weeks";
  version = "0.1.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
                               aeson
                               base
                               blaze-html
                               diagrams
                               diagrams-cairo
                               diagrams-lib
                               directory
                               doctemplates
                               http-types
                               pandoc
                               scotty
                               scotty-cookie
                               split
                               text
                               time
                               wai-extra
                               warp
                             ];
  prePatch = "hpack";
  homepage = "https://5218weeks.tobidot.eu/";
  description = "Personal Your-Life-in-Weeks-Calendar";
  license = lib.licenses.agpl3Only;
}
