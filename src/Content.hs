{-# LANGUAGE OverloadedStrings #-}
-- For Help on how to use the Pandoc api confer
-- https://pandoc.org/using-the-pandoc-api.html
--
-- In short:
-- do
--   result <- runIO $ do
--     pandoc stoff
--   handleError result
module Content
( index
, index404
) where

import Text.Pandoc
import qualified Data.Text as T
import Data.Aeson -- contains object
import Text.DocTemplates -- contains toContext, compileTemplateFile

getHtmlOpts :: String -> IO WriterOptions
getHtmlOpts pandoc_template = do
    template <- rightToMaybe <$> compileTemplateFile pandoc_template
    return $ def
      { writerTemplate = template
      , writerVariables = variables
        }

variables :: Context T.Text
variables = toContext $ object [
              "css" .= ([ "/node_modules/normalize.css/normalize.css"
                        , "/node_modules/milligram/dist/my_milligram.css"
                        , "/css/main.css"
                        , "/node_modules/@fengyuanchen/datepicker/dist/datepicker.css"
                        , "/node_modules/@fortawesome/fontawesome-free/css/all.css"
                        ] :: [String])
            , "js" .= ([ "/node_modules/jquery/dist/jquery.js"
                       , "/node_modules/@fengyuanchen/datepicker/dist/datepicker.js"
                       , "/node_modules/moment/moment.js"
                       , "/js/about-nav.js"
                       , "/js/download-animation.js"
                       , "/js/disable-button.js"
                       ] :: [String])
            , "title" .= ("5218weeks" :: String)
            , "gitlab" .= ("https://gitlab.com/tobidot/5218weeks" :: String)
            , "domain" .= ("http://5218weeks.tobidot.eu" :: String)
            , "impressum" .= ("https://tobidot.eu/about" :: String)
            , "privacy" .=  ("https://tobidot.eu/legal/privacy-policy" :: String)
            ]

parse :: String -> WriterOptions -> IO String
parse s opts = do 
  text <- runIO $ do
    doc <- readMarkdown def (T.pack s)
    writeHtml5String opts doc
  result <- handleError text
  return (T.unpack result)


index :: IO String
index = do
    markdown <- readFile "./static/about.md"
    opts <- getHtmlOpts "./static/index.pandoc_template"
    parse markdown opts

index404 :: IO String
index404 = do
    markdown <- readFile "./static/about.md"
    opts <- getHtmlOpts "./static/index404.pandoc_template"
    parse markdown opts

rightToMaybe :: Either a b -> Maybe b
rightToMaybe = either (const Nothing) Just
