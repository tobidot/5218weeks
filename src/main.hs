{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Web.Scotty.Cookie
import qualified Lifeweeks as L
import qualified Content as C
import qualified Data.Text.Lazy as T
import qualified Network.HTTP.Types.Status as S
import Network.Wai.Handler.Warp (defaultSettings, Port, setPort)


main :: IO ()
main = do
  putStrLn "Live at http://127.0.0.1:32323"  
  scottyServe 32323 $ do
    -- middleware logStdoutDev
    get "/" $ do
      i <- liftAndCatchIO C.index
      html $ T.pack i
    get "/download" $ do
      bday_text <- param "bday"
      setSimpleCookie "DownloadStatus" "done"
      maybeIOToActionM $ L.getCalendar "static/calendars/" bday_text
      downloadPdf bday_text
    get "/img/:image" $ do
      img <- param "image"
      addContentType img
      file $ "static/img/" ++ T.unpack img
    get "/css/:css" $ do
      css <- param "css"
      addContentType ".css"
      file $ "static/css/" ++ T.unpack css
    get "/js/:js" $ do
      js <- param "js"
      addContentType ".js"
      file $ "static/js/" ++ T.unpack js
    get (regex "^/node_modules/(.*)$") $ do
      path <- param "1"
      addContentType path
      file $ "static/node_modules/" ++ T.unpack path
    notFound $ do
      i <- liftAndCatchIO C.index404
      html $ T.pack i
      status S.notFound404

addContentType :: T.Text -> ActionM ()
addContentType filepath
  | T.isSuffixOf ".svg" filepath  = addHeader "Content-Type" "image/svg+xml"
  | T.isSuffixOf ".jpg" filepath  = addHeader "Content-Type" "image/jpeg"
  | T.isSuffixOf ".jpeg" filepath = addHeader "Content-Type" "image/jpeg"
  | T.isSuffixOf ".png" filepath  = addHeader "Content-Type" "image/png"
  | T.isSuffixOf ".css" filepath  = addHeader "Content-Type" "text/css"
  | T.isSuffixOf ".js" filepath   = addHeader "Content-Type" "text/javascript"
  | otherwise                     = addHeader "Content-Type" "text/plain"

maybeIOToActionM :: Maybe (IO ()) -> ActionM()
maybeIOToActionM (Just io) = liftAndCatchIO io
maybeIOToActionM Nothing = status S.notFound404

scottyServe :: Port -> ScottyM () -> IO ()
scottyServe port = scottyOpts Options {verbose = 0
                                       , settings = setPort port defaultSettings}

-- headers from
-- http://stackoverflow.com/questions/364946/how-to-make-pdf-file-downloadable-in-html-link
downloadPdf :: String -> ActionM()
downloadPdf bday_text = do
  setHeader "Content-Description" "File Transfer"
  setHeader "Content-Type" "application/octet-stream"
  setHeader "Content-Type" "application/force-download"
  setHeader "Content-Disposition" $ T.pack ("attachment; filename=yourLifeInWeeks_" ++ bday_text ++ ".pdf")
  setHeader "Content-Transfer-Encoding" "binary"
  setHeader "Expires" "0"
  setHeader "Cache-Control" "must-revalidate, post-check=0, pre-check=0"
  setHeader "Pragma" "public"
  -- setHeader "Content-Length" " . filesize($file));
  file $ "static/calendars/" ++ bday_text ++ ".pdf"
