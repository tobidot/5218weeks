module Tests where

import qualified Lifeweeks as L
import Data.Time              -- date library

life :: [Day]
life = L.lifeWeeks birthday eventualDeath

mbday :: Day
mbday = fromGregorian 2005 11 16

mdday :: Day
mdday = fromGregorian 2105 11 16

birthday ::  Day
birthday = fromGregorian 1991 05 13

eventualDeath :: Day
eventualDeath = fromGregorian 2091 05 13

testDate :: Day
testDate = fromGregorian 2888 12 30

