-- Language Extensions

{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}

module Lifeweeks 
( getCalendar
, lifeWeeks
, createPoster
, createNotCachedPoster
, countWeeks
) where

-- Import Librarys

import Control.Monad
import Diagrams.Prelude       -- Diagrams
import Diagrams.Backend.Cairo -- Cairo Backend
import Data.Time              -- date library
import Data.List.Split        -- for splitOn
import Text.Read
import qualified Data.Time.Calendar as C
import System.Directory (doesFileExist)


-- USAGE
-- `myrenderPDF $ completePoster bday dday`


-- Actual Program

-- Create Selfcontained executable file
-- Deprecated for now
--main = mainWith (dayCorners birth)

getCalendar :: FilePath -> String -> Maybe (IO ())
getCalendar path_prefix bday_text = do
    bday <- parseGregorian bday_text
    return $ createNotCachedPoster path_prefix bday


createNotCachedPoster :: String -> Day -> IO ()
createNotCachedPoster path_prefix bday = do
    isCached <- calendarExists path_prefix bday
    Control.Monad.unless isCached $ createPoster path_prefix bday


createPoster :: String -> Day -> IO ()
createPoster path_prefix bday =
    renderCairo path poster_width (completePoster bday dday)
        where dday = addGregorianYearsClip 100 bday
              poster_width = mkWidth (25*53/7.6 * 10)
              path = pdfPathFromDate path_prefix bday


pdfPathFromDate :: String -> Day -> String
pdfPathFromDate path_prefix bday = path_prefix ++ C.showGregorian bday ++ ".pdf"

-- see https://rosettacode.org/wiki/Check_that_file_exists#Haskell
calendarExists :: String -> Day -> IO Bool
calendarExists path_prefix bday =
    doesFileExist $ pdfPathFromDate path_prefix bday

-- Simple Geometric Shapes

recTriangle :: Double -> Diagram B
recTriangle n = polygon ( with
  & polyType .~ PolySides
      [ 90 @@ deg, 45 @@ deg ]
      [ n        , n         ]
  )

upperLeftCorner :: Double -> QDiagram B V2 Double Any
upperLeftCorner  n = recTriangle      n # rotate (45 @@ deg)

lowerLeftCorner :: Double -> QDiagram B V2 Double Any
lowerLeftCorner  n = upperLeftCorner  n # rotate (90 @@ deg)

lowerRightCorner :: Double -> QDiagram B V2 Double Any
lowerRightCorner n = lowerLeftCorner  n # rotate (90 @@ deg)

--Constants
defaultCs :: Double
defaultCs = 0.5

defaultFs :: Double
defaultFs = 0.15

headingFontSize :: Double
headingFontSize = 6

headingWidth :: Double
headingWidth    = 53

headingHeight :: Double
headingHeight   = 6

heading :: String
heading = "DON'T PANIC!"

--Text with correct fontsize

yearText :: Double -> Day -> QDiagram B V2 Double Any
yearText fs day  = text (show $ getYear  day) # fontSize (local fs)

monthTextAligned :: Double -> Double -> Double -> Day -> QDiagram B V2 Double Any
monthTextAligned x y fs day = alignedText x y (show $ getMonth  day) # fontSize (local fs)

dayTextAligned :: Double -> Double -> Double -> Day -> QDiagram B V2 Double Any
dayTextAligned x y fs day   = alignedText x y (show $ getDay  day) # fontSize (local fs)

--The individual corners with text on top

monthCornerLeft :: Double -> Double -> Day -> QDiagram B V2 Double Any
monthCornerLeft fs cs day  = monthTextAligned 0.6 0.3 fs day <> upperLeftCorner cs

dayCornerAligned :: Double -> Double -> Day -> QDiagram B V2 Double Any
dayCornerAligned fs cs day = dayTextAligned 0.4 0.7 fs day <> lowerRightCorner cs

-- Boxes content
genericWeekBoxContent :: Day -> Day -> Day -> QDiagram B V2 Double Any
genericWeekBoxContent birth death day
   | isNewYear day = weekBoxContent day <> (yearText 0.3 day # rotate (45 @@ deg) # opacity 0.2)
   | otherwise     = weekBoxContent day
          where isNewYear x = elem x $ newYearsWeeks birth death

weekBoxContent :: Day -> QDiagram B V2 Double Any
weekBoxContent day =
  ((monthCornerLeft fs cs day ||| (strutX cs # withEnvelope (upperLeftCorner cs)))
   ===
   ((strutX cs # withEnvelope (lowerLeftCorner cs)) ||| dayCornerAligned fs cs day))
  # center
      where cs = defaultCs
            fs = defaultFs


-- complete WeekBox
weekBox :: Day -> Day -> Day -> QDiagram B V2 Double Any
weekBox birth death day = (genericWeekBoxContent birth death day <> square 1) # lwG 0.05

-- boxes at beginning of line
ageBox :: Double -> Int -> QDiagram B V2 Double Any
ageBox fs age = text (show age) # fontSize (local fs) <> square 1 # lwG 0.05

allAgeBoxes :: Double -> Int -> [QDiagram B V2 Double Any]
allAgeBoxes fs l= map  (ageBox fs) [0..l-1]

-- all weeks
weekBoxes :: Day -> Day -> [[QDiagram B V2 Double Any]]
weekBoxes birth death =
   map (map (weekBox birth death)) weekMatrix
      where weekMatrix = weeksPerYear birth death

fullGrid :: Day -> Day -> QDiagram B V2 Double Any
fullGrid birth death =
   vcat ( map hcat fullRow)
      where weekBoxesList = weekBoxes birth death
            years         = length weekBoxesList
            fullRow       = zipWith (:) (allAgeBoxes 0.3 years) weekBoxesList

-- heading
headingRendered :: Diagram B
headingRendered = text heading # fontSize (local headingFontSize)
                              # bold
                              # font "Droid Sans"
              <> strutX headingWidth <> strutY headingHeight

-- legend
legendLine :: Diagram B
legendLine = (circle 2 # lwG 0.05) === strutY 0.5

headingOffset :: Double
headingOffset = height headingRendered + 1 --0.5 from padding

legend :: Int -> QDiagram B V2 Double Any
legend n = strutY headingOffset === vcat (replicate n legendLine)

legendcircles :: Diagram B -> Int
legendcircles mainDiagram = floor ( (h - 2*headingOffset)/(2 + 2 + 0.5)) +1
                      where h = height mainDiagram

-- Prettyfy
poster :: Day -> Day -> QDiagram B V2 Double Any
poster birth death = headingRendered ===
                     padDiagramm 0 1 0.5 0 (fullGrid birth death) # center

posterWithLegend :: Day -> Day -> QDiagram B V2 Double Any
posterWithLegend birth death = mainpart ||| legend n
                    where mainpart = poster birth death
                          n        = legendcircles mainpart

completePoster :: Day -> Day -> Diagram B
completePoster birth death = padDiagramm 1 1 1 1 $ posterWithLegend birth death

-- Generally useful Diagram functions
padDiagramm :: Double -> Double -> Double -> Double -> Diagram B -> Diagram B
padDiagramm left right top bottom diagram =
    strutY top ===
    ( strutX left ||| diagram ||| strutX right) ===
    strutY bottom


-- Date Stuff
parseGregorian :: String -> Maybe Day
parseGregorian yyy_mm_dd = fromGregorianValidList $ mapM readMaybe splitDate
    where splitDate = splitOn "-" yyy_mm_dd


fromGregorianValidList :: Maybe [Int] -> Maybe Day
fromGregorianValidList (Just [x1, x2, x3]) = fromGregorianValid (toInteger x1) x2 x3
fromGregorianValidList (Just _)            = Nothing
fromGregorianValidList Nothing           = Nothing


-- basics

nextWeek :: Day -> Day
nextWeek = addDays 7

nextYear :: Day -> Day
nextYear = addGregorianYearsClip 1

getYear :: Day -> Integer
getYear day = (\(x,_,_) -> x ) $ toGregorian day

getMonth :: Day -> Int
getMonth day = (\(_,x,_) -> x ) $ toGregorian day

getDay :: Day -> Int
getDay day = (\(_,_,x) -> x ) $ toGregorian day

-- stuff

countWeeks :: String -> String -> Maybe Int
countWeeks startString endString = do
                s <- parseGregorian startString
                e <- parseGregorian endString
                return $ length $ lifeWeeks s e

lifeWeeks :: Day -> Day -> [Day]
lifeWeeks birth death = takeWhile (< death) allWeeks
                where allWeeks = iterate nextWeek birth

lifeYears :: Day -> Day -> [Day]
lifeYears birth death =
      takeWhile (<death) allYears
                where allYears = iterate nextYear birth

lifeNewYears :: Day -> Day -> [Day]
lifeNewYears birth death =
      takeWhile (<death) $ iterate nextYear firstNewYear
         where firstNewYear = fromGregorian (1 + getYear birth) 1 1

newYearsWeeks :: Day -> Day -> [Day]
newYearsWeeks birth death =
     map head $ binBy weeks newYears
     where weeks    = lifeWeeks birth death
           newYears = lifeNewYears birth death

weeksPerYear :: Day -> Day -> [[Day]]
weeksPerYear birth death = binBy weeks years
      where weeks = lifeWeeks birth death
            years = drop 1 $ lifeYears birth death

-- General Functions

binBy :: (Ord a) => [a] -> [a] -> [[a]]
binBy [] _ = [[]]
binBy xs [] = [xs]
binBy xs (y:ys) = smaller:binBy rest ys
   where smaller = takeWhile (<y) xs
         rest  = dropWhile (<y) xs
