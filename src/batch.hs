module Main where

import Data.Time              -- date library
import Control.Monad
import System.Environment
import System.IO
import qualified Lifeweeks as L

main :: IO ()
main = do
  args <- getArgs
  let n = read (head args)
  forM_ (lastBirthdays n) (\day -> do
    L.createNotCachedPoster "./static/calendars/" day
    putStr "."
    hFlush stdout)
  putStrLn "|"

lastBirthdays ::  Int -> [Day]
lastBirthdays n = take n $ iterate (addDays (-1)) startDay
        where startDay = fromGregorian 2010 01 01



