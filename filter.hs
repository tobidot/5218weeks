#!/usr/bin/env runhaskell
-- behead2.hs
import Text.Pandoc.JSON

main :: IO ()
main = toJSONFilter make_relative
  where make_relative (Image a b (url, c)) | (url !! 0 == '/') = Image a b ("./static" ++ url, c)
        make_relative x = x
