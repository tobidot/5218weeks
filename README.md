# Your life in 5218 Weeks

## Install build environment

Debian/Ubuntu:

    sudo apt install stack
    stack upgrade

Add the following to your `.bashrc`:

```bash
export PATH=~/.local/bin/:${PATH}
```

Install `npm` and `nodejs` following ([DO](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server))

    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    sudo apt install nodejs

This is necessary as the Ubuntu14.04 nodejs version is too old

## Install dependencies

    sudo apt install libcairo-dev
    sudo apt install libpango1.0-dev
    # if not working try
    # sudo apt install libghc-pango-dev


## Install js libraries

    cd static
    npm install @fengyuanchen/datepicker
    npm install jquery

## Install css framework

    cd static
    npm install milligram

## Install

    stack build

## Run

    stack exec life

# Nixos

## Installation/Deployment using nix

As the `5218weeks.nix` file is using a pinned git commit for building 5218weeks code changes currently can only be deployed if commited to the repo.

### Updating the pinned Git Commit

Run 

```sh
$ nix-prefetch-git https://gitlab.com/tobidot/5218weeks.git
...
{
  "url": "https://gitlab.com/tobidot/5218weeks.git",
  "rev": "1aa0cb35b5e1ea96b94b1391f8776a8453f1c2d8",
  "date": "2021-04-17T22:50:41+02:00",
  "sha256": "047lhgqziarnf4z0qbiqzvr02fdryn4w4nk50pnj98l5cfhxwzgk",
  "fetchSubmodules": false
}
```

and use the information from the last block to update the equivalent `fetchGit` section in the `5218weeks.nix` file.

### Building

This is as painless as usual with nix: `nix build`
The resulting binaries can be found in `./result/bin/`.

### Deployment using systemctl and nixos

You can use the following `.nix` file in your `/etc/nixos` directory to automatically start 5218weeks on system boot.

```nix
{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    stack zlib cairo pango haskellPackages.zlib-bindings
  ];

  services.nginx = {
    enable = true;
    virtualHosts = {
      "<yourvirtualhost" = {
        forceSSL = true;
        enableACME = true;
        locations."/" = {
          proxyPass = "http://127.0.0.1:32323";
        };
      };
    };
  };

  systemd.services.weeks = {
      wantedBy = [ "multi-user.target" ]; 
      after = [ "network.target" ];
      description = "Start the 5218weeks webserver.";
      serviceConfig = {
        WorkingDirectory = "<yourbasedir>/5218weeks";
        Type = "simple";
        User = "tobidot";
        StandardOutput = "journal";
        ExecStart = "<yourbasedir>/5218weeks/result/bin/life";
        ExecStop = "${pkgs.killall}/bin/killall life";
      };
   };
}
```

and import it according in your `/etc/nixos/configuration.nix` file using

```nix
  imports =
    [ 
      ...
      ./5218weeks.nix
      ...
    ];
```


## Installation/Deployment with Stack

The build process takes up to 1G of space in `/tmp`.
Thus make sure that your user tmpfs at `/run/user/$uid` has enough space.

If your default `tmpfs` is to small you can temporarily set another tmp dir via the environment variable `TMPDIR`:

    export TMPDIR=...

If you think you need more space in general you can also add the following to your `/etc/nixos/configuration.nix`:

    services.logind.extraConfig = "RuntimeDirectorySize=1G"

For a in depth description of the possible values see `man 5 logind.conf`.


## Disclaimer

The pandoc engine as currently implemented does not support fenced Codeblocks.
This could be changed by following the instructions from the [Pandoc Guide](https://pandoc.org/using-the-pandoc-api.html#options) by adding the extension
[fenced codeblocks](https://pandoc.org/MANUAL.html#fenced-code-blocks).
Since codeblocks are only used once this is not done now as the [4 space intended codeblocks](https://pandoc.org/MANUAL.html#indented-code-blocks) are sufficient.

